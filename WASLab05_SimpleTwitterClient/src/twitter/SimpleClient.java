package twitter;

import java.util.Date;

import twitter4j.FilterQuery;
import twitter4j.ResponseList;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		/*final Twitter twitter = new TwitterFactory().getInstance();
		
		Date now = new Date();
		//String latestStatus = "I want to increase the Klout score of @cfarre [task #4 completed "+now+"]";
		//Status status = twitter.updateStatus(latestStatus);
		//System.out.println("Successfully updated the status to: " + status.getText());
		long userId = twitter.showUser("fib_was").getId();
		ResponseList<Status> response = twitter.getUserTimeline(userId);
		System.out.println(response.get(0).getText());
		twitter.retweetStatus(response.get(0).getId());*/
		TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		StatusListener statusListener = new StatusListener() {

	         @Override
	         public void onStatus(Status status) {
	        	 System.out.println(status.getUser().getName() + " (@" + status.getUser().getScreenName() + "): " + status.getText());
	         }


	                @Override
	                public void onDeletionNotice(StatusDeletionNotice sdn) {
	                    throw new UnsupportedOperationException("Not supported yet."); 
	                }

	                @Override
	                public void onTrackLimitationNotice(int i) {
	                    throw new UnsupportedOperationException("Not supported yet."); 
	                }

	                @Override
	                public void onScrubGeo(long l, long l1) {
	                    throw new UnsupportedOperationException("Not supported yet."); 
	                }

	                @Override
	                public void onStallWarning(StallWarning sw) {
	                    throw new UnsupportedOperationException("Not supported yet.");
	                }


					@Override
					public void onException(Exception arg0) {
						// TODO Auto-generated method stub
						
					}
	            };

	            FilterQuery fq = new FilterQuery();        

	            String keywords[] = {"#barcelona"};

	            fq.track(keywords);        

	            twitterStream.addListener(statusListener);
	            twitterStream.filter(fq);    
	}
}
